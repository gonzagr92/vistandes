  # This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
I18n.locale = :es

User.new(email: 'gonza.gr92@gmail.com', password: 'm2n-x36930115', password_confirmation: 'm2n-x36930115');

gradum = Category.create(title: "Gradum",
description: "Máxima expresión de los vinos elaborados en la bodega, categoría Premium, a partir de una cuidadosa selección de las uvas celosamente supervisado por nuestro departamento de enología, utilizando varietales de diferentes cosechas, logrando conformar un vino con características distintivas y complejas. En las variedades Malbec y Cabernet Sauvignon con uvas de finca “Hechizo del Plata” de Mendoza; y Carmenère de finca “Viña Famatina” de La Rioja.",
image: "http://www.vistandes.com/images/vinos-gradum.png")
reserve = Category.create(title: "Reserve",
description: "Nuestro primer vino de guarda, donde se busca amalgamar el terroir con un sutil acompañamiento de roble. Esta línea representa el terroir de nuestra finca Hechizo del Plata en sus varietales Malbec y Cabernet Sauvignon, y del Valle de Famatina en un distintivo Carmenère. La elegancia del Malbec se combina con las especias del Carmenère en un excelente bi-varietal, propuesta exclusiva de nuestra bodega.",
image: "http://www.vistandes.com/images/vinos-reserve.png")
vistandes = Category.create(title: "Vistandes",
description: "Vinos jóvenes caracterizados por ser frescos y modernos, en los que se busca resaltar la presencia de la fruta y la tipicidad de cada uno de los varietales utilizados. Los varietales Malbec y Cabernet Sauvignon, se obtienen de la conjunción de ambos viñedos,  Mendoza y Valle de Famatina, en tanto que los varietales Carmenère y  Torrontés se elaboran con uvas seleccionadas únicamente del viñedo del Valle de Famatina.",
image: "http://www.vistandes.com/images/vinos-vistandes.png")

#GRADUM

gradum_carmenere = Wine.create(title: "Carménère",
description_terroir: "<p> Producido con las uvas Carménère del Valle de Famatina, <strong>La Rioja</strong> y un pequeño porcentaje de <strong>Maipu, Mendoza.</strong></p><p> Suelos franco-arenosos, con presencia de piedras y buen drenaje natural.</p>",
description_taste: "<p> <strong>Aromas</strong> exóticos de frutas del bosque, regaliz, pimienta negra y clavos de olor.</p><p> La crianza en barricas de roble le aporta agradables fondos tostados y de vainilla.</p><p> El <strong>paladar</strong> es elegante, con taninos dulces y frutales que entregan matices de cuero y especias.</p><p> Perfecto con carnes de ternera como asi tambien con guisos estofados. <strong>Acompaña</strong> bien postres con frutas.</p>",
recommendation:'<p>Recomendamos consumirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td> </tr><tr><td><h5> Azúcar Residual: </h5><td><h5> 1.90 g/l</h5></td> </tr><tr><td><h5> Acidez Total: </h5></td><td><h5> 6.1 g/l</h5></td> </tr><tr><td><h5>PH: </h5></td><td><h5> 3.24</h5></td> </tr></table>',
image: "http://www.vistandes.com/images/gradum-carmenere.png",
category: gradum)

gradum_cabernet = Wine.create(title: "Cabernet Sauvignon",
description_terroir: "<p>Producido con uvas de nuestro viñedo de <strong>Maipu, Mendoza</strong>, complementado con un porcentaje del Valle de Famatina, La Rioja . </p><p>Suelos franco-arenosos, con presencia de piedras y buen drenaje natural. </p>",
description_taste: "<p><strong>Color</strong> púrpura profundo e intenso, con destellos granates.  </p><p>Su <strong>aroma</strong> presenta un perfil frutado y expresivo, con berries maduros, especias y toques de café y tostado provenientes de su crianza en barricas de roble. </p><p><strong>Cuerpo</strong> robusto, con un paladar frutado y complejo, presentando notas de casís y frambuesa. Vino redondo, con taninos firmes y pulidos. </p> <p>Este Cabernet Sauvignon <strong>marida</strong> bien con carnes rojas grilladas y salsas agridulces. En general, es un buen compañero de comidas bien condimentadas. </p>",
recommendation: '<p>Recomendamos consumirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5> 1.90 g/l</h5></td></tr><tr><td><h5> Acidez Total: </h5></td><td><h5> 6.1 g/l</h5></td></tr><tr><td><h5>PH: </h5></td><td><h5> 3.24</h5></td></tr></table>',
image: 'http://www.vistandes.com/images/gradum-cabernet-sauvignon.png',
category: gradum)

gradum_malbec = Wine.create(title: "Malbec",
description_terroir: "<p>Vino producido con uvas Malbec de nuestros viñedos propios ubicados en Cruz de Piedra, <strong>Maipu, Mendoza.</strong></p><p>Suelo franco-arenoso, con presencia de piedras y buen drenaje natural .</p>",
description_taste: "<p><strong>Color</strong> rojo intenso con marcados destellos violeta. </p><p><strong>Aromas</strong> a ciruelas maduras y flores de violetas. Cuenta con notas de chocolate y mocha provenientes de su crianza en barricas de roble.</p><p>Vino concentrado, de <strong>paladar</strong> jugoso y elegante, con taninos suaves y redondeados. Su balanceada acidez le otorga frescura y un final persistente.</p><p><strong>Marida</strong> muy bien con carnes de caza, platos con verduras grilladas y salsas no muy picantes. También es gran compañero de postres con base de chocolate.</p>",
recommendation: '<p>Recomendamos consumirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5> 2.15 g/l</h5></td></tr><tr><td><h5> Acidez total: </h5></td><td><h5> 5.94 g/l</h5></td></tr><tr><td><h5>PH: </h5></td><td><h5> 3.43</h5></td></tr></table>',
image: 'http://www.vistandes.com/images/gradum-malbec.png',
category: gradum)

# RESERVE

reserve_carmenere = Wine.create(title: "Carménère Malbec",
description_terroir: "<p> Producido con 80% de uvas Carménère del Valle de Famatina, La Rioja y 20% de Malbec de nuestro viñedo Hechizo del Plata de <strong>Maipu, Mendoza.</strong></p> <p> Suelo franco-arenoso, con buen drenaje natural. </p>",
description_taste: "<p> <strong>Color</strong> púrpura intenso y profundo, con leves destellos granates. </p> <p> <strong>Aromas</strong> exóticos, muy expresivo. Presencia de berries maduros y frutos del bosque complementados con notas a frutos rojos del Malbec, que se integran con café y tostado de su paso por barricas de roble. </p> <p> Taninos bien pulidos y redondos, con <strong>cuerpo</strong> medio y final muy agradable. </p> <p> Este blend <strong>combina</strong> bien con carnes grilladas con guarnición de salsas agridulces.  </p>",
recommendation: '<p>Recomendamos consumirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5> 13.5% </h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5> 1.87 g/l</h5></td></tr><tr><td><h5> Acidez Total: </h5></td><td><h5> 5.69 g/l</h5></td></tr><tr><td><h5>PH: </h5></td><td><h5> 3.62</h5></td></tr></table>',
image: 'http://www.vistandes.com/images/reserve-carmenere-malbec.png',
category: reserve)

reserve_malbec = Wine.create(title: "Malbec",
description_terroir: "<p> Producido con uvas Malbec de nuestro viñedo Hechizo del Plata, ubicado en <strong>Maipu, Mendoza.</strong> </p><p> Suelo franco-arenoso, con buen drenaje natural.  </p>",
description_taste: "<p> A la vista presenta un <strong>color</strong> violeta profundo y vivaz. </p><p> <strong>Cuerpo</strong> medio, con buena intensidad aromática.  </p><p> Se perciben notas de ciruelas y moras, complementadas con <strong>aromas</strong> a chocolate y ahumado provenientes de su paso por barricas de roble. </p><p> Taninos redondos y persistentes que amalgamados con la presencia de roble crean un final firme y persistente. </p><p> <strong>Marida</strong> muy bien con carnes rojas, pastas y quesos con salsas livianas. </p>",
recommendation: '<p>Recomendamos servirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.9%</h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5>1.87 g/l</h5></td></tr><tr><td><h5> Acidez Total: </h5></td><td><h5>5.6 g/l</h5></td></tr><tr><td><h5>PH:</h5></td><td><h5>3.67</h5></td></tr></table>',
image: 'http://www.vistandes.com/images/reserve-malbec.png',
category: reserve)

reserve_cabernet = Wine.create(title: "Cabernet Sauvignon",
description_terroir: "<p> Producido con uvas Cabernet Sauvignon de nuestro viñedo Hechizo del Plata, ubicado en <strong>Maipu, Mendoza.</strong></p><p> Suelo franco-arenoso, con buen drenaje natural. </p>",
description_taste: "<p> <strong>Color</strong> rojo rubí con destellos violeta.</p><p> Este vino despliega <strong>aromas</strong> a frutos rojos combinados con especias y pimientos, resaltadas por sus notas ahumadas provenientes de su paso por barricas de roble.</p><p> En <strong>boca</strong> se presenta suave pero concentrado, con buena textura y taninos refinados. Final largo. </p><p> Es un excelente <strong>compañero</strong> de carnes fuertes como cordero; salsas de hongos, y estofados.</p>",
recommendation: '<p>Recomendamos servirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>14.5% </h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5>1.87 g/l</h5></td></tr><tr><td><h5> Acidez Total: </h5></td><td><h5>5.6 g/l</h5></td></tr><tr><td><h5>PH:</h5></td><td><h5>3.67</h5></td></tr></table>',
image: 'http://www.vistandes.com/images/reserve-cabernet-sauvignon.png',
category: reserve)

#Vistandes
vistandes_malbec = Wine.create(title: "Malbec",
description_terroir: "<p>Vino producido con uvas Malbec seleccionadas de nuestros viñedos propios ubicados en <strong>Maipu, Mendoza.</strong></p>",
description_taste: "<p>Presenta un <strong>color</strong> violeta intenso, con reflejos violeta.</p><p>Es un vino con buen balance y equilibrio, con marcados <strong>aromas</strong> frutales destacando las ciruelas y las notas especiadas.</p><p>En boca presenta un <strong>cuerpo</strong> medio, con taninos redondos y amenos.</p><p><strong>Acompaña</strong> muy bien a carnes rojas grilladas, estofados y pastas.</p>",
recommendation: '<p>Recomendamos consumirlo entre 9° y 11°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.9% </h5></td></tr><tr><td><h5> Azúcar Residual: </h5></td><td><h5>1.88 g/l </h5></td></tr><tr><td><h5> Acidez Total: </h5></td><td><h5>5.79 g/l </h5></td></tr><tr><td><h5>PH:</h5></td><td><h5>3.56 </h5></td></tr></table>',
image: 'http://www.vistandes.com/images/vistandes-malbec.png',
category: vistandes)

vistandes_rose = Wine.create(title: "Malbec Rose",
description_terroir: "<p>Vino producido con uvas Malbec de nuestros viñedos propios ubicados en Cruz de Piedra, <strong>Maipu, Mendoza.</strong></p>",
description_taste: "<p>Presenta un <strong>color</strong> rosado intenso y viváz.</p><p>Este Rosado de Malbec sorprende con sus marcadas notas a guindas, frambuesas y frutillas, y sutiles notas de violetas.</p><p><strong>Paladar</strong> fresco y jugoso, con buena acidez. De estructura media, tiene un final crujiente y persistente.</p><p>Este <strong>rosado<strong> va muy bien con pescados, pasta con salsas livianas, sushi y ensaladas.</p>",
recommendation: '<p>Recomendamos servirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.5% </h5></td></tr><tr><td><h5> Azucar Residual: </h5></td><td><h5>1.88 g/l </h5></td></tr></table>',
image: 'http://www.vistandes.com/images/vistandes-malbec-rose.png',
category: vistandes)

vistandes_cabernet = Wine.create(title: "Cabernet Sauvignon",
description_terroir: "<p>Vino producido con uvas Cabernet Sauvignon de viñedos seleccionados de <strong>Mendoza.</strong></p>",
description_taste: "<p>Presenta un <strong>color</strong> rojo profundo e intenso.</p><p>Es un vino muy <strong>expresivo</strong>, con marcadas notas a especias y pimientos, aunque también se percibe la presencia de frutos rojos.</p><p>En boca presenta un <strong>cuerpo</strong> medio, con taninos redondos y amenos; y marcada presencia de frutas rojas maduras.</p><p><strong>Acompaña</strong> muy bien a carnes rojas bien condimentadas, estofados y pastas con salsas especiadas.</p>",
recommendation: '<p>Recomendamos servirlo entre 16° y 18°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.5% </h5></td></tr><tr><td><h5> Azucar Residual: </h5></td><td><h5>1.88 g/l </h5></td></tr></table>',
image: 'http://www.vistandes.com/images/vistandes-cabernet-sauvignon.png',
category: vistandes)

vistandes_torrontes = Wine.create(title: "Torrontes",
description_terroir: "<p>Vino producido con uvas Torrontes de nuestro viñedo ubicado en <strong>La Rioja.</strong></p>",
description_taste: "<p>Presenta un <strong>color</strong> amarillo profundo con leves destellos dorados.</p><p> Es un vino muy expresivo, <strong>aromático</strong>, se destacan sus notas a flores blancas y frutas cítricas.</p><p>En boca presenta un <strong>cuerpo</strong> medio, fresco, con una muy buena acidez que le otorga un final largo y agradable.</p><p><strong>Acompaña</strong> muy bien ensaladas, frutos de mar y comida Asiática.</p>",
recommendation: '<p>Recomendamos servirlo entre 8° y 10°C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.9% </h5></td></tr><tr><td><h5> Azucar Residual: </h5></td><td><h5>2.1g/l </h5></td></tr></table>',
image: 'http://www.vistandes.com/images/vistandes-torrontes.png',
category: vistandes)

hechizo = Finca.create(name: 'Hechizo del Plata',
description: 'Finca “Hechizo del Plata”, ubicada en el corazón del distrito Cruz de Piedra en el departamento de Maipú,  conocida históricamente como la Primera Zona Vitivinícola de la Provincia de Mendoza.')

famatina = Finca.create(name: 'Viña Famatina',
description: 'Finca “Viña Famatina”, ubicada en el departamento de Chilecito, en pleno Valle de Famatina, localidad de Anguinan, Provincia de La Rioja, a una altitud de 1050 metros sobre el nivel del mar.')

FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo2.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo1.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo6.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo3.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo4.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo5.jpg', finca: hechizo)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/hechizo7.jpg', finca: hechizo)

FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina1.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina2.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina3.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina4.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina5.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina6.jpg', finca: famatina)
FincaImg.create(image: 'http://www.vistandes.com/images/fincas/famatina7.jpg', finca: famatina)


I18n.locale = :en

hechizo.update(name: 'Hechizo del Plata',
description: '"Hechizo del Plata" farm, located in the heart of the district Cruz de Piedra in the Department of Maipu, place known historically as the first wine area of the province of Mendoza.')

famatina.update(name: 'Viña Famatina',
description: 'Estate "Viña Famatina", located in the Department of Chilecito, in the Famatina Valley, village of Anguinán, province of La Rioja, at an altitude of 1050 meters above sea level.')

gradum.update(description: "Maximum expression of all wines produced in the winery, Premium category, from a careful selection of the grapes jealously supervised by our Department of Oenology, using varieties from different harvests, achieving a distinctive and complex wine. The grapes for the Malbec and Cabernet Sauvignon varieties come from our estate located in  Mendoza “Hechizo del Plata”; and the variety Carmenere from our estate in La Rioja “Viña Famatina”.")
reserve.update(description: "These wines seek to amalgamate the “terroir” with a delicate accompaniment of oak. This line represents the “terroir” of our estate Hechizo de Plata (Silver Spell) in their varieties Malbec and Cabernet Sauvignon, and the Famatina Valley in a distinctive Carmenere. The elegance of the Malbec variety is combined with Carmenere spices in an excellent addition, proposed exclusively from our wine cellar. ")
vistandes.update(description: "Young wines characterized by being fresh and modern, which seeks to highlight the presence of the fruit and the typicity of each of the varietals used. The Malbec and Cabernet Sauvignon varietals, are obtained from the conjunction of both vineyards, Mendoza and Famatina Valley, whereas Carmenere and Torrontés varietals are produced only with selected grapes from the Famatina Valley Vineyard.")

#Gradum
gradum_carmenere.update(description_terroir: "<p> Made with Carmenere grapes from the Famatina Valley, <strong>La Rioja</strong> and a small percentage from <strong>Maipú, Mendoza.</strong></p><p>Sandy soils, presence of stones and good natural drainage.</p>",
description_taste: "<p>Cherry <strong>intense</strong> and deep color, with slight Garnet flashes. </p><p>Exotic aromas of fruits of the forest, licorice, black pepper, and clove. Ageing in oak barrels gives nice toasted and vanilla flavors to this wine.</p><p>In the <strong>palate</strong> is elegant, with sweet and fruity tannins that deliver hints of leather and spices. </p><p>Perfect to pair with meat from beef and also with stewed dishes. Good <strong>partner</strong> for desserts with fruit.</p>",
recommendation:'<p>We recommend consuming between 16 ° and 18 ° C</p><table>	<tr>	<td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td></tr><tr><td><h5> Residual Sugar: </h5><td><h5> 1.90 g/l</h5></td></tr><tr><td>	<h5> Total Acidity: </h5></td><td><h5> 6.1 g/l</h5></td></tr>	<tr><td><h5>PH: </h5></td><td><h5> 3.24</h5></td></tr></table>')

gradum_cabernet.update(description_terroir: "<p>Made with grapes from our vineyards in <strong>Maipu, Mendoza</strong>, and grapes from the Famatina Valley, La Rioja.</p><p>Sandy soils, presence of stones and good natural drainage.</p>",
description_taste: "<p> Purple deep and intense <strong>color</strong>, with Garnet flashes. </p><p> In the nose has fruity and expressive <strong>aromas</strong>, with ripe berries, spices and touches of coffee and toast from aging the wine in oak barrels. </p><p> <strong>Robust</strong> body, with a fruity and complex palate, with notes of blackcurrant and Raspberry. Balanced wine with firm and polished tannins. </p><p> This Cabernet Sauvignon <strong>pairs</strong> well with grilled red meats and sweet and sour sauces. In general, is a good partner for well seasoned food. </p>",
recommendation: '<p>We recommend consuming between 16 ° and 18 ° C </p><table>	<tr><td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td>	</tr>	<tr><td><h5> Residual Sugar: </h5></td><td><h5> 1.90 g/l</h5></td>	</tr>	<tr><td><h5> Total Acidity: </h5></td><td><h5> 6.1 g/l</h5></td>	</tr>	<tr><td><h5>PH: </h5></td><td><h5> 3.24</h5></td>	</tr></table>')

gradum_malbec.update(description_terroir: "<p>Wine made with Malbec grapes from our own vineyards located in Cruz de Piedra, <strong>Maipu, Mendoza.</strong></p><p>Sandy loam soil, presence of stones and good natural drainage.</p>",
description_taste: "<p>Intense <strong>red color</strong> with purple flashes. </p><p><strong>Aromas</strong> of ripe plums and violet flowers. Has notes of chocolate and mocha from aging in oak barrels. </p><p>Palate juicy and <strong>elegant</strong>, with soft and rounded tannins. Its balanced acidity gives freshness and a persistent finish. </p><p>It <strong>pairs</strong> very well with meats of hunting, dishes with grilled vegetables and not very spicy sauces. Is also great partner for chocolate-based desserts.</p>",
recommendation: '<p>We recommend consuming between 16 ° and 18 ° C</p><table>	<tr><td><h5> Alcohol: </h5></td><td><h5> 13.9% </h5></td>	</tr>	<tr><td><h5> Residual Sugar: </h5></td><td><h5> 2.15 g/l</h5></td>	</tr>	<tr><td><h5> Total Acidity: </h5></td><td><h5> 5.94 g/l</h5></td>	</tr>	<tr><td><h5>PH: </h5></td><td><h5> 3.43</h5></td></tr></table>')

#Reserve
reserve_carmenere.update(description_terroir: "<p>Produced with 80% of Carménère grapes from the Famatina Valley, La Rioja and 20% from our Malbec vineyards in the state “Hechizo del Plata” (Silver Spell), <strong>Maipu, Mendoza.</strong> </p><p>Sandy loam soil, with good natural drainage.</p>",
description_taste: "<p>This wine shows a <strong>purple intense</strong> and deep color, with slight garnet flashes. <p><p>Exotic and very expressive <strong>aromas</strong>. Presence of ripe berries and fruits of the forest complemented by notes of red fruits of the Malbec, along with coffee and toast added by its aging in oak barrels. <p><p>With rounded tannins in mouth this is a <strong>medium-body</strong> wine with a nice finish. <p><p>This <strong>blend</strong> goes well with grilled meats in sweet and sour sauces. <p>",
recommendation: '<p>We recommend consuming between 16 ° and 18 ° C</p><table>	<tr><td><h5> Alcohol: </h5></td><td><h5> 13.5% </h5></td>	</tr>	<tr><td><h5> Residual Sugar: </h5></td><td><h5> 1.87 g/l</h5></td>	</tr>	<tr><td><h5> Total Acidity: </h5></td><td><h5> 5.69 g/l</h5></td>	</tr>	<tr><td><h5>PH: </h5></td><td><h5> 3.62</h5></td>	</tr></table>')

reserve_malbec.update(description_terroir: "<p>Produced with Malbec grapes from our vineyard “Hechizo del Plata” (Silver Spell), located in Maipú, Mendoza </p><p>Sandy loam soil, with good natural drainage.</p>",
description_taste: "<p>In the sight shows a deep and lively <strong>violet.</strong> </p><p>Wine with good aromatic intensity, notes of plums and blackberries, complemented with <strong>aromas</strong> of chocolate and smoked from its passage by oak barrels. </p><p>Round and persistent tannins that amalgamated with the presence of oak, create a firm and persistent finish. </p><p><strong>Pairs</strong> very well with red meat, pasta and cheese with light sauces. </p>",
recommendation: '<p>We recommend to serve between 16 ° and 18 ° C</p><table>	<tr><td><h5> Alcohol: </h5></td><td><h5>13.9%</h5></td>	</tr>	<tr><td><h5> Residual Sugar: </h5></td><td><h5>1.87 g/l</h5></td>	</tr>	<tr><td><h5> Total Acidity: </h5></td><td><h5>5.6 g/l</h5></td>	</tr>	<tr><td><h5>PH:</h5></td><td><h5>3.67</h5></td>	</tr></table>')

reserve_cabernet.update(description_terroir: "<p>Produced with Cabernet Sauvignon grapes from our vineyard “Hechizo del Plata” (Silver spell), located in <strong>Maipu, Mendoza.</strong> </p><p>Sandy loam soil, with good natural drainage.</p>",
description_taste: "<p>This wine shows a <strong>ruby red colour</strong> with glints violet. </p><p>This wine displays <strong>aromas</strong> of red fruit combined with spices and peppers, highlighted by its smoky notes from of aging through oak barrels. </p><p><strong>Mouth</strong> is soft but focused, with good texture and refined tannins. Long finish. </p><p>It is an excellent <strong>partner</strong> for strong meat as lamb; sauces of fungi, and stews. </p>",
recommendation: '<p>We recommend to serve between 16 ° and 18 ° C</p><table>	<tr><td><h5> Alcohol: </h5></td><td><h5>14.5% </h5></td>	</tr>	<tr><td><h5> Residual Sugar: </h5></td><td><h5>1.87 g/l</h5></td>	</tr>	<tr><td><h5> Total Acidity: </h5></td><td><h5>5.6 g/l</h5></td>	</tr>	<tr><td><h5>PH:</h5></td><td><h5>3.67</h5></td>	</tr></table>')

#Vistandes

vistandes_malbec.update(description_terroir: "<p>Wine produced with the Malbec grapes selected from our own vineyards located in <strong>Maipu, Mendoza.</strong> </p>",
description_taste: "<p>It <strong>shows</strong> a deep violet colour, with violet reflexes. </p><p>It is a wine with good balance and <strong>equilibrium</strong>, with marked fruity flavors, highlighting plums and spiced notes. </p><p>In the mouth it´s a <strong>medium-body</strong> wine, with round and mild tannins. </p><p><strong>Goes</strong> very well to grilled red meats, stews and pastas. </p>",
recommendation: '<p>We recommend to serve between 16 ° and 18 ° C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.9% </h5></td></tr><tr><td><h5> Residual Sugar: </h5></td><td><h5>1.88 g/l </h5></td></tr><tr><td><h5> Total Acidity: </h5></td><td><h5>5.79 g/l </h5></td></tr><tr><td><h5>PH:</h5></td><td><h5>3.56 </h5></td></tr></table>')

vistandes_rose.update(description_terroir: "<p>Wine produced with Malbec grapes from our own vineyards located in Cruz de Piedra, <strong>Maipú, Mendoza.</strong></p>",
description_taste: "<p>It shows a lively and intense <strong>pink color.</strong> </p><p>This Rosé of Malbec surprises with its marked notes to cherries, raspberries and strawberries, and subtle notes of violets. </p><p>The <strong>palate</strong> is fresh and juicy, with good acidity. Medium structure, and a crisp, persistent finish. </p><p>This <strong>rosé wine<s/trong> goes very well with fish, pasta with light sauces, sushi and salads.</p>",
recommendation: '<p>We recommend to serve between 16 ° and 18 ° C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.5% </h5></td></tr><tr><td><h5> Residual Sugar: </h5></td><td><h5>1.88 g/l </h5></td></tr></table>')

vistandes_cabernet.update(description_terroir: "<p>Wine produced with Cabernet Sauvignon grapes from <strong>selected</strong> vineyards.</p>",
description_taste: "<p>Shows a deep and intense red <strong>color</strong>. </p><p>It is a wine that is very <strong>expressive</strong>, with marked notes of spices and peppers, but you can also perceive the presence of red fruit.  </p><p>In the mouth it´s a <strong>medium-body</strong> wine, with rounded enjoyable tannin sand marked presenceof red ripe fruits.  </p><p>It <strong>goes</strong> very well with seasoned red meats, stew and pasta with spicy sauces. </p>",
recommendation: '<p>We recommend to serve between 16 ° and 18 ° C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.5% </h5></td></tr><tr><td><h5> Residual Sugar: </h5></td><td><h5>1.88 g/l </h5></td></tr></table>')

vistandes_torrontes.update(description_terroir: "<p>Wine produced with Torrontes grapes from our vineyard located in <strong>La Rioja.</strong></p>",
description_taste: "<p>It shows a deep yellow <strong>colour</strong> with slight gold sparkles. </p><p>It is a very expressive, <strong>aromatic</strong> wine, their notes of white flowers and citrus fruits stand out. </p><p>In the mouth it is a <strong>medium-bodied wine</strong>, fresh, with good acidity that gives a long, pleasant finish. </p><p><strong>Goes</strong> very well with salads, seafood and Asian food. </p>",
recommendation: '<p>We recommend to serve between 8 ° and 10 ° C</p><table><tr><td><h5> Alcohol: </h5></td><td><h5>13.9% </h5></td></tr><tr><td><h5> Residual Sugar: </h5></td><td><h5>2.1g/l </h5></td></tr></table>')
