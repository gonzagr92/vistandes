class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.string :title
      t.text :description

      t.timestamps null: false
    end
    reversible do |change|
      change.up do
        add_attachment :categories, :image
      end

      change.down do
        remove_attachment :categories, :image
      end
    end
  end
end
