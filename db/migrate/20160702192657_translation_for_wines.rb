class TranslationForWines < ActiveRecord::Migration
  def up
    Wine.create_translation_table!({description_terroir: :text, description_taste: :text, recommendation: :text}, {migrate_data: true})
  end

  def down
    Wine.drop_translation_table! migrate_data: true
  end
end
