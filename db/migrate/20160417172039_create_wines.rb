class CreateWines < ActiveRecord::Migration
  def change
    create_table :wines do |t|
      t.string :title
      t.text :description_terroir
      t.text :description_taste
      t.text :recommendation
      t.references :category, index: true, foreign_key: true

      t.timestamps null: false
    end
    reversible do |change|
      change.up do
        add_attachment :wines, :image
      end

      change.down do
        remove_attachment :wines, :image
      end
    end
  end
end
