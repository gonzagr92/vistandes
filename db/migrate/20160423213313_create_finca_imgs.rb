class CreateFincaImgs < ActiveRecord::Migration
  def change
    create_table :finca_imgs do |t|
      t.references :finca, index: true, foreign_key: true
      t.timestamps null: false
    end
    reversible do |change|
      change.up do
        add_attachment :finca_imgs, :image
      end
      change.down do
        remove_attachment :finca_imgs, :image
      end
    end
  end
end
