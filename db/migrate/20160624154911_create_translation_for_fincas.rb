class CreateTranslationForFincas < ActiveRecord::Migration
  def up
    Finca.create_translation_table!({name: :string, description: :text}, {migrate_data: true})
  end

  def down
    Finca.drop_translation_table! migrate_data: true
  end

end
