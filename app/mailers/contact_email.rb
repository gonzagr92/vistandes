class ContactEmail < ApplicationMailer
  def contacto(mail_params)
    @email = mail_params['email']
    @nombre = mail_params['nombre']
    @mensaje = mail_params['mensaje']
    @asunto = mail_params['asunto']
    mail(from: '"Vistandes Web" <web@vistandes.com>', to: 'info@vistandes.com, administracion@vistandes.com', subject: mail_params['asunto'])
  end
end
