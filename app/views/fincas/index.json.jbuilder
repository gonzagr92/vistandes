json.array!(@fincas) do |finca|
  json.extract! finca, :id
  json.url finca_url(finca, format: :json)
end
