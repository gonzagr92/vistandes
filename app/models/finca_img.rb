class FincaImg < ActiveRecord::Base
  belongs_to :finca

  has_attached_file :image, styles: { original: "1280", medium: "600"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
end
