class Category < ActiveRecord::Base
  has_many :wines
  has_attached_file :image, styles: { medium: "500"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  translates :description

end
