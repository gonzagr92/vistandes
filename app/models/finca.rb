class Finca < ActiveRecord::Base
  has_many :finca_imgs
  translates :name, :description
  before_save { |t| t.uid = t.name.downcase.dasherize.tr(' ','-') }

end
