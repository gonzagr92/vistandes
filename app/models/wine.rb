class Wine < ActiveRecord::Base
  belongs_to :category

  has_attached_file :image, styles: { original: "500", medium: "300"}
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\Z/
  translates :description_terroir, :description_taste, :recommendation
end
