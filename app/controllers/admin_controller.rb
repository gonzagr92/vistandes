class AdminController < ApplicationController
  layout "admin"
  skip_before_action :restrict_access, only: [:index]
  before_action :authenticate_user!

  def index
    
  end

end
