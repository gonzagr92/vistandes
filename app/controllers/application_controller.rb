class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  before_action :restrict_access
  before_action :set_locale

  def set_locale
    if cookies[:vistandes_locale] && I18n.available_locales.include?(cookies[:vistandes_locale].to_sym)
      lang = cookies[:vistandes_locale].to_sym
    else
      lang = I18n.default_locale
      cookies.permanent[:vistandes_locale] = lang
    end
    I18n.locale = lang
  end

  def restrict_access
    redirect_to root_url unless user_signed_in?
  end
end
