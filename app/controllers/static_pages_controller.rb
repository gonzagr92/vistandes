class StaticPagesController < ApplicationController
  skip_before_action :restrict_access
  before_action :set_category
  before_action :set_fincas

  def index

  end

  def contacto

  end

  def quienes_somos

  end

  def fincas
    @fincas = Finca.all.includes(:finca_imgs)
  end

  def finca_single
    @finca = Finca.find_by(uid: params[:uid])
    redirect_to root_url if @finca.blank?
  end

  def vinos
    @categories = Category.all
  end

  def vinos_single
    @wines = Category.includes(:wines).find_by(title: params[:category_title])
    redirect_to root_url if @wines.blank?
  end

  def eventos
  end

  def contacto_mail
    respond_to do |format|
      ContactEmail.contacto(mail_params).deliver_now
      format.html { redirect_to contacto_url, notice: 'Gracias por enviarnos su mensaje.' }
    end
  end

  def change_locale
    lang = params[:locale].to_sym
    lang = I18n.default_locale unless I18n.available_locales.include?(lang)
    cookies.permanent[:vistandes_locale] = lang
    redirect_to request.referer || root_url
  end

  private
    def set_category
      @categories = Category.select(:title)
    end

    def set_fincas
      @fincas_head = Finca.all
    end

    def mail_params
      params.require(:contact_form).permit(:nombre, :email, :asunto, :mensaje)
    end
end
