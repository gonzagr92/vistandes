Rails.application.routes.draw do

  scope 'admin' do
    resources :fincas
  end

  root 'static_pages#index'
  get 'contacto' => 'static_pages#contacto'
  get 'quienes_somos' => 'static_pages#quienes_somos'
  get 'fincas_home' => 'static_pages#fincas'
  get 'vinos_home' => 'static_pages#vinos'
  get 'vinos_single' => 'static_pages#vinos_single'
  get 'turismo_y_eventos' => 'static_pages#eventos'
  get 'admin' => 'admin#index'
  post 'contact_mail' => 'static_pages#contacto_mail'

  get '/fincas/:uid' => 'static_pages#finca_single'
  get '/change_locale/:locale' => 'static_pages#change_locale'
  get '/:category_title' => 'static_pages#vinos_single'

  devise_for :users, controllers: {sessions: "sessions"}

end
