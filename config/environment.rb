# Load the Rails application.
require File.expand_path('../application', __FILE__)

# Initialize the Rails application.
Rails.application.initialize!

ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :address        => ENV['MAILER_ADDRESS'],
  :port           => 25,
  :authentication => :plain,
  :user_name      => ENV['MAILER_USERNAME'],
  :password       => ENV['MAILER_PASSWORD'],
  :enable_starttls_auto => true,
  :openssl_verify_mode => 'none'
}
